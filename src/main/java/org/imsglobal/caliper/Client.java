/**
 * This file is part of IMS Caliper Analytics™ and is licensed to
 * IMS Global Learning Consortium, Inc. (http://www.imsglobal.org)
 * under one or more contributor license agreements.  See the NOTICE
 * file distributed with this work for additional information.
 *
 * IMS Caliper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * IMS Caliper is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.imsglobal.caliper;

import com.fasterxml.jackson.databind.JsonNode;
import org.imsglobal.caliper.entities.Entity;
import org.imsglobal.caliper.events.Event;
import org.imsglobal.caliper.request.ApacheHttpRequestor;
import org.imsglobal.caliper.request.HttpUrlRequestor;
import org.imsglobal.caliper.request.Requestor;
import org.imsglobal.caliper.stats.Statistics;
import org.imsglobal.caliper.validators.SensorValidator;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Caliper Client - Instantiate this to use the Caliper Sensor API.
 * 
 * The client is an HTTP wrapper over the Caliper REST API. It will allow
 * you to conveniently consume the API without making any HTTP requests
 * yourself.
 *
 * This client is designed to be thread-safe and to not block each call in
 * order to make a HTTP request.
 */
public class Client {
    private String id;
    private Options options;
    private Statistics stats;
    private Requestor.RequestorImpl requestor;

    /**
     * Default constructor
     */
    public Client() {
        this.requestor = Requestor.RequestorImpl.APACHE;
    }

    /**
     * Initialize Client with a Client identifier and configurable options. This will use the Apache HttpClient backend
     * for network requests
     *
     * @param id
     * @param options
     */
    public Client(String id, Options options) {
        this(id, options, Requestor.RequestorImpl.APACHE);
    }

    /**
     * Initializes Client with a given identifier, configurable options, and will use the specified network library
     * to make http requests.
     *
     * @param id
     * @param options
     * @param requestorType
     */
    public Client(String id, Options options, Requestor.RequestorImpl requestorType) {
        SensorValidator.checkClientId(id);
        SensorValidator.checkOptions(options);
        SensorValidator.checkApiKey(options.getApiKey());
        SensorValidator.checkHost(options.getHost());

        this.id = id;
        this.options = options;
        this.requestor = requestorType;
        this.stats = new Statistics();
    }

    /**
     * Get the Client identifier.
     * @return client identifier
     */
    @Nonnull
    public String getId() {
        return id;
    }

    /**
     * Set the Client identifier.
     * @param id
     */
    public void setId(@Nonnull String id) {
        this.id = id;
    }

    /**
     * Get the configuration options.
     * @return options
     */
    @Nonnull
    public Options getOptions() {
        return options;
    }

    /**
     * Set the Configuration options.
     * @param options
     */
    public void setOptions(@Nonnull Options options) {
        this.options = options;
    }

    /**
     * Send a single Entity describes to a target event store.
     * @param sensor
     * @param data
     */
    public void describe(Sensor sensor, Entity data) throws IOException {
        List<Entity> dataList = new ArrayList<>();
        dataList.add(data);

        describe(sensor, dataList);
    }

    /**
     * Send a collection of Entity describes to a target event store.
     * @param sensor
     * @param data
     */
    public void describe(Sensor sensor, List<Entity> data) throws IOException {
        Requestor<Entity> requestor = requestorForType(this.requestor, options);
        try {
            updateStats(requestor.send(sensor, data));
        } catch (IOException e) {
            updateStats(false);
            throw e;
        }
    }

    /**
     * Send a single event to a target event store.
     * @param sensor
     * @param data
     */
    public void send(Sensor sensor, Event data) throws IOException {
        List<Event> dataList = new ArrayList<>();
        dataList.add(data);

        send(sensor, dataList);
    }

    /**
     * Send a collection of events to a target event store.
     * @param sensor
     * @param data
     */
    public void send(Sensor sensor, List<Event> data) throws IOException {
        Requestor<Event> requestor = requestorForType(this.requestor, options);
        try {
            updateStats(requestor.send(sensor, data));
        } catch (IOException e) {
            updateStats(false);
            throw e;
        }
    }

    /**
     * Send json representation of an event to a target event store.
     * @param sensor
     * @param json
     */
    public void sendJson(Sensor sensor, JsonNode json) throws IOException {
        List<JsonNode> jsonList = new ArrayList<>(1);
        jsonList.add(json);

        sendJson(sensor, jsonList);
    }

    /**
     * Send json representation of events to a target event store.
     * @param sensor
     * @param json
     */
    public void sendJson(Sensor sensor, List<JsonNode> json) throws IOException {
        Requestor<JsonNode> requestor = requestorForType(this.requestor, options);
        try {
            updateStats(requestor.send(sensor, json));
        } catch (IOException e) {
            updateStats(false);
            throw e;
        }
    }

    /**
     * Get statistics.
     * @return statistics
     */
    @Nonnull
    public Statistics getStatistics() {
        return this.stats;
    }

    /**
     * Update stats
     * @param status
     */
    private void updateStats(boolean status) {
        this.stats.updateMeasures(1);

        if (status) {
            stats.updateSuccessful(1);
        } else {
            stats.updateFailed(1);
        }
    }

    private <T> Requestor<T> requestorForType(Requestor.RequestorImpl requestorType, Options options) {
        switch (requestorType) {
            case HTTP_URL:
                return new HttpUrlRequestor<>(options);
            case APACHE:
            default:
                return new ApacheHttpRequestor<>(options);
        }
    }
}