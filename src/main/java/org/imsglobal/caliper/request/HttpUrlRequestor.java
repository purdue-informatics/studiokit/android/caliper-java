package org.imsglobal.caliper.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.imsglobal.caliper.Options;
import org.imsglobal.caliper.Sensor;
import org.imsglobal.caliper.databind.JsonObjectMapper;
import org.imsglobal.caliper.payload.Envelope;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HttpUrlRequestor<T> extends Requestor<T> {
    private Options options;
    private static final Logger log = LoggerFactory.getLogger(ApacheHttpRequestor.class);

    /**
     * Constructor instantiates a new OkHTTPRequestor. The args options provides the host
     * details to the HttpClient.
     * @param options
     */
    public HttpUrlRequestor(Options options) {
        super();
        this.options = options;
    }

    /**
     * Get the configurable options
     * @return the options
     */
    public Options getOptions() {
        return options;
    }

    /**
     * Set the configurable options.
     * @param options
     */
    public void setOptions(Options options) {
        this.options = options;
    }


    @Override
    public boolean send(Sensor sensor, T data) throws IOException {
        List<T> dataList = new ArrayList<>(1);
        dataList.add(data);

        return send(sensor, dataList);
    }

    @Override
    public boolean send(Sensor sensor, List<T> data) throws IOException {
        boolean status = Boolean.FALSE;

        if (log.isDebugEnabled()) {
            log.debug("Entering send()...");
        }

        ObjectMapper mapper = JsonObjectMapper.create(options.getJsonInclude());

        // Create envelope, serialize it as a JSON string.
        Envelope<T> envelope = createEnvelope(sensor, DateTime.now(), data);

        // Serialize envelope with mapper
        String json = serializeEnvelope(envelope, mapper);

        HttpURLConnection connection = (HttpURLConnection) new URL(options.getHost()).openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Authorization", options.getApiKey());
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setConnectTimeout(25000);
        connection.setReadTimeout(15000);


        OutputStream output = null;
        try {
            output = connection.getOutputStream();
            output.write(json.getBytes());
        } finally {
            try {
                if (output != null)
                    output.close();
            } catch (IOException e2) {
                if (log.isDebugEnabled()) {
                    log.debug(e2.getMessage());
                }
            }
        }

        int responseCode = connection.getResponseCode();

        if (!(responseCode >= 200 && responseCode < 300)) {
            readStreamAndClose(connection.getErrorStream());
            throw new RuntimeException("Failed : HTTP error code : " + responseCode);
        } else {
            if (log.isDebugEnabled()) {
                log.debug(String.valueOf(responseCode));
                //Catch the IOException here since the HTTP request was successful and this is just logging
                try {
                    log.debug(readStreamStringAndClose(connection.getInputStream()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    readStreamAndClose(connection.getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            status = Boolean.TRUE;

            if (log.isDebugEnabled()) {
                log.debug("Exiting send()...");
            }
        }

        return status;
    }

    /**
     * Read through and close the InputStream returned by an HTTP request to allow HttpURLConnection to reuse the connection
     *
     * @param inputStream
     */
    private void readStreamAndClose(InputStream inputStream) {
        if (inputStream == null)
            return;

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            //noinspection StatementWithEmptyBody
            while (reader.skip(Long.MAX_VALUE) > 0);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Read the input stream returned by an HTTP request. The given InputStream will be closed.
     *
     * @param inputStream
     * @return  String representation of the provided InputStream
     */
    private String readStreamStringAndClose(InputStream inputStream) {
        if (inputStream == null)
            return "";

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String line;
        try {
            while ( (line = reader.readLine()) != null) {
                builder.append(line);
                builder.append('\n');
            }

            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
